<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Country controller.
 *
 * @Route("admin")
 */
class AdminController extends Controller
{
  /**
   * Lists all country entities.
   *
   * @Route("/", name="homeAdmin")
   * @Method("GET")
   */
    public function indexAction()
    {

        return $this->render('AppBundle:admin:index.html.twig');
    }

}
