<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MarksteineType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Marksteinetype controller.
 *
 * @Route("admin/marksteinetype")
 */
class MarksteineTypeController extends Controller
{
    /**
     * Lists all marksteineType entities.
     *
     * @Route("/", name="marksteinetype_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $marksteineTypes = $em->getRepository('AppBundle:MarksteineType')->findAll();

        return $this->render('AppBundle:marksteinetype:index.html.twig', array(
            'marksteineTypes' => $marksteineTypes,
        ));
    }

    /**
     * Creates a new marksteineType entity.
     *
     * @Route("/new", name="marksteinetype_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $marksteineType = new Marksteinetype();
        $form = $this->createForm('AppBundle\Form\MarksteineTypeType', $marksteineType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($marksteineType);
            $em->flush();

            return $this->redirectToRoute('marksteinetype_show', array('id' => $marksteineType->getId()));
        }

        return $this->render('AppBundle:marksteinetype:new.html.twig', array(
            'marksteineType' => $marksteineType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a marksteineType entity.
     *
     * @Route("/{id}", name="marksteinetype_show")
     * @Method("GET")
     */
    public function showAction(MarksteineType $marksteineType)
    {
        $deleteForm = $this->createDeleteForm($marksteineType);

        return $this->render('AppBundle:marksteinetype:show.html.twig', array(
            'marksteineType' => $marksteineType,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing marksteineType entity.
     *
     * @Route("/{id}/edit", name="marksteinetype_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MarksteineType $marksteineType)
    {
        $deleteForm = $this->createDeleteForm($marksteineType);
        $editForm = $this->createForm('AppBundle\Form\MarksteineTypeType', $marksteineType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('marksteinetype_edit', array('id' => $marksteineType->getId()));
        }

        return $this->render('AppBundle:marksteinetype:edit.html.twig', array(
            'marksteineType' => $marksteineType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a marksteineType entity.
     *
     * @Route("/{id}", name="marksteinetype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MarksteineType $marksteineType)
    {
        $form = $this->createDeleteForm($marksteineType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($marksteineType);
            $em->flush();
        }

        return $this->redirectToRoute('marksteinetype_index');
    }

    /**
     * Creates a form to delete a marksteineType entity.
     *
     * @param MarksteineType $marksteineType The marksteineType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MarksteineType $marksteineType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('marksteinetype_delete', array('id' => $marksteineType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
