<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Period;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Period controller.
 *
 * @Route("admin/period")
 */
class PeriodController extends Controller
{
    /**
     * Lists all period entities.
     *
     * @Route("/", name="period_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $periods = $em->getRepository('AppBundle:Period')->findAll();

        return $this->render('AppBundle:period:index.html.twig', array(
            'periods' => $periods,
        ));
    }

    /**
     * Creates a new period entity.
     *
     * @Route("/new", name="period_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $period = new Period();
        $form = $this->createForm('AppBundle\Form\PeriodType', $period);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($period);
            $em->flush();

            return $this->redirectToRoute('period_show', array('id' => $period->getId()));
        }

        return $this->render('AppBundle:period:new.html.twig', array(
            'period' => $period,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a period entity.
     *
     * @Route("/{id}", name="period_show")
     * @Method("GET")
     */
    public function showAction(Period $period)
    {
        $deleteForm = $this->createDeleteForm($period);

        return $this->render('AppBundle:period:show.html.twig', array(
            'period' => $period,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing period entity.
     *
     * @Route("/{id}/edit", name="period_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Period $period)
    {
        $deleteForm = $this->createDeleteForm($period);
        $editForm = $this->createForm('AppBundle\Form\PeriodType', $period);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('period_edit', array('id' => $period->getId()));
        }

        return $this->render('AppBundle:period:edit.html.twig', array(
            'period' => $period,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a period entity.
     *
     * @Route("/{id}", name="period_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Period $period)
    {
        $form = $this->createDeleteForm($period);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($period);
            $em->flush();
        }

        return $this->redirectToRoute('period_index');
    }

    /**
     * Creates a form to delete a period entity.
     *
     * @param Period $period The period entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Period $period)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('period_delete', array('id' => $period->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
