<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Government;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Government controller.
 *
 * @Route("admin/government")
 */
class GovernmentController extends Controller
{
    /**
     * Lists all government entities.
     *
     * @Route("/", name="government_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $governments = $em->getRepository('AppBundle:Government')->findAll();

        return $this->render('AppBundle:government:index.html.twig', array(
            'governments' => $governments,
        ));
    }

    /**
     * Creates a new government entity.
     *
     * @Route("/new", name="government_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $government = new Government();
        $form = $this->createForm('AppBundle\Form\GovernmentType', $government);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($government);
            $em->flush();

            return $this->redirectToRoute('government_show', array('id' => $government->getId()));
        }

        return $this->render('AppBundle:government:new.html.twig', array(
            'government' => $government,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a government entity.
     *
     * @Route("/{id}", name="government_show")
     * @Method("GET")
     */
    public function showAction(Government $government)
    {
        $deleteForm = $this->createDeleteForm($government);

        return $this->render('AppBundle:government:show.html.twig', array(
            'government' => $government,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing government entity.
     *
     * @Route("/{id}/edit", name="government_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Government $government)
    {
        $deleteForm = $this->createDeleteForm($government);
        $editForm = $this->createForm('AppBundle\Form\GovernmentType', $government);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('government_edit', array('id' => $government->getId()));
        }

        return $this->render('AppBundle:government:edit.html.twig', array(
            'government' => $government,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a government entity.
     *
     * @Route("/{id}", name="government_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Government $government)
    {
        $form = $this->createDeleteForm($government);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($government);
            $em->flush();
        }

        return $this->redirectToRoute('government_index');
    }

    /**
     * Creates a form to delete a government entity.
     *
     * @param Government $government The government entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Government $government)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('government_delete', array('id' => $government->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
