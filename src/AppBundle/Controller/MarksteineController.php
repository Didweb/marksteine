<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Marksteine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Marksteine controller.
 *
 * @Route("admin/marksteine")
 */
class MarksteineController extends Controller
{
    /**
     * Lists all marksteine entities.
     *
     * @Route("/", name="marksteine_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $marksteines = $em->getRepository('AppBundle:Marksteine')->findAll();

        return $this->render('AppBundle:marksteine:index.html.twig', array(
            'marksteines' => $marksteines,
        ));
    }

    /**
     * Creates a new marksteine entity.
     *
     * @Route("/new", name="marksteine_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $marksteine = new Marksteine();
        $form = $this->createForm('AppBundle\Form\MarksteineType', $marksteine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($marksteine);
            $em->flush();

            return $this->redirectToRoute('marksteine_show', array('id' => $marksteine->getId()));
        }

        return $this->render('AppBundle:marksteine:new.html.twig', array(
            'marksteine' => $marksteine,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a marksteine entity.
     *
     * @Route("/{id}", name="marksteine_show")
     * @Method("GET")
     */
    public function showAction(Marksteine $marksteine)
    {
        $deleteForm = $this->createDeleteForm($marksteine);

        return $this->render('AppBundle:marksteine:show.html.twig', array(
            'marksteine' => $marksteine,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing marksteine entity.
     *
     * @Route("/{id}/edit", name="marksteine_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Marksteine $marksteine)
    {
        $deleteForm = $this->createDeleteForm($marksteine);
        $editForm = $this->createForm('AppBundle\Form\MarksteineType', $marksteine);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('marksteine_edit', array('id' => $marksteine->getId()));
        }

        return $this->render('AppBundle:marksteine:edit.html.twig', array(
            'marksteine' => $marksteine,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a marksteine entity.
     *
     * @Route("/{id}", name="marksteine_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Marksteine $marksteine)
    {
        $form = $this->createDeleteForm($marksteine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($marksteine);
            $em->flush();
        }

        return $this->redirectToRoute('marksteine_index');
    }

    /**
     * Creates a form to delete a marksteine entity.
     *
     * @param Marksteine $marksteine The marksteine entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Marksteine $marksteine)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('marksteine_delete', array('id' => $marksteine->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
