<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Government
 *
 * @ORM\Table(name="government")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GovernmentRepository")
 */
class Government
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updateAt;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="governments")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;


    /**
     * @var int
     *
     * @ORM\Column(name="yearInit", type="integer")
     */
    private $yearInit;

    /**
     * @var int
     *
     * @ORM\Column(name="monthInit", type="integer")
     */
    private $monthInit;

    /**
     * @var int
     *
     * @ORM\Column(name="dayInit", type="integer")
     */
    private $dayInit;

    /**
     * @var int
     *
     * @ORM\Column(name="yearEnd", type="integer")
     */
    private $yearEnd;

    /**
     * @var int
     *
     * @ORM\Column(name="monthEnd", type="integer")
     */
    private $monthEnd;

    /**
     * @var int
     *
     * @ORM\Column(name="dayEnd", type="integer")
     */
    private $dayEnd;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param string $createdAt
     *
     * @return Government
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updateAt
     *
     * @param string $updateAt
     *
     * @return Government
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return string
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Government
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Government
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set yearInit
     *
     * @param integer $yearInit
     *
     * @return Government
     */
    public function setYearInit($yearInit)
    {
        $this->yearInit = $yearInit;

        return $this;
    }

    /**
     * Get yearInit
     *
     * @return int
     */
    public function getYearInit()
    {
        return $this->yearInit;
    }

    /**
     * Set monthInit
     *
     * @param integer $monthInit
     *
     * @return Government
     */
    public function setMonthInit($monthInit)
    {
        $this->monthInit = $monthInit;

        return $this;
    }

    /**
     * Get monthInit
     *
     * @return int
     */
    public function getMonthInit()
    {
        return $this->monthInit;
    }

    /**
     * Set dayInit
     *
     * @param integer $dayInit
     *
     * @return Government
     */
    public function setDayInit($dayInit)
    {
        $this->dayInit = $dayInit;

        return $this;
    }

    /**
     * Get dayInit
     *
     * @return int
     */
    public function getDayInit()
    {
        return $this->dayInit;
    }

    /**
     * Set yearEnd
     *
     * @param integer $yearEnd
     *
     * @return Government
     */
    public function setYearEnd($yearEnd)
    {
        $this->yearEnd = $yearEnd;

        return $this;
    }

    /**
     * Get yearEnd
     *
     * @return int
     */
    public function getYearEnd()
    {
        return $this->yearEnd;
    }

    /**
     * Set monthEnd
     *
     * @param integer $monthEnd
     *
     * @return Government
     */
    public function setMonthEnd($monthEnd)
    {
        $this->monthEnd = $monthEnd;

        return $this;
    }

    /**
     * Get monthEnd
     *
     * @return int
     */
    public function getMonthEnd()
    {
        return $this->monthEnd;
    }

    /**
     * Set dayEnd
     *
     * @param integer $dayEnd
     *
     * @return Government
     */
    public function setDayEnd($dayEnd)
    {
        $this->dayEnd = $dayEnd;

        return $this;
    }

    /**
     * Get dayEnd
     *
     * @return int
     */
    public function getDayEnd()
    {
        return $this->dayEnd;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return Government
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
