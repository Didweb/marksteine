<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryRepository")
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="countrys")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="Marksteine", mappedBy="country", cascade={"persist"})
     */
    protected $marksteines;

    /**
     * @ORM\OneToMany(targetEntity="Government", mappedBy="country", cascade={"persist"})
     */
    protected $governments;




    public function __construct()
    {
        $this->marksteines = new ArrayCollection();
        $this->governments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set region
     *
     * @param \AppBundle\Entity\Region $region
     *
     * @return Country
     */
    public function setRegion(\AppBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \AppBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Add marksteine
     *
     * @param \AppBundle\Entity\Marksteine $marksteine
     *
     * @return Country
     */
    public function addMarksteine(\AppBundle\Entity\Marksteine $marksteine)
    {
        $this->marksteines[] = $marksteine;

        return $this;
    }

    /**
     * Remove marksteine
     *
     * @param \AppBundle\Entity\Marksteine $marksteine
     */
    public function removeMarksteine(\AppBundle\Entity\Marksteine $marksteine)
    {
        $this->marksteines->removeElement($marksteine);
    }

    /**
     * Get marksteines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarksteines()
    {
        return $this->marksteines;
    }

    /**
     * Add government
     *
     * @param \AppBundle\Entity\Government $government
     *
     * @return Country
     */
    public function addGovernment(\AppBundle\Entity\Government $government)
    {
        $this->governments[] = $government;

        return $this;
    }

    /**
     * Remove government
     *
     * @param \AppBundle\Entity\Government $government
     */
    public function removeGovernment(\AppBundle\Entity\Government $government)
    {
        $this->governments->removeElement($government);
    }

    /**
     * Get governments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGovernments()
    {
        return $this->governments;
    }
}
