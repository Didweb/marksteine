<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * MarksteinType
 *
 * @ORM\Table(name="marksteine_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MarksteineTypeRepository")
 */
class MarksteineType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;


    /**
     * @ORM\OneToMany(targetEntity="Marksteine", mappedBy="type", cascade={"persist"})
     */
    protected $marksteines;


    public function __construct()
    {
        $this->marksteines = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return MarksteinType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add marksteine
     *
     * @param \AppBundle\Entity\Marksteine $marksteine
     *
     * @return MarksteineType
     */
    public function addMarksteine(\AppBundle\Entity\Marksteine $marksteine)
    {
        $this->marksteines[] = $marksteine;

        return $this;
    }

    /**
     * Remove marksteine
     *
     * @param \AppBundle\Entity\Marksteine $marksteine
     */
    public function removeMarksteine(\AppBundle\Entity\Marksteine $marksteine)
    {
        $this->marksteines->removeElement($marksteine);
    }

    /**
     * Get marksteines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarksteines()
    {
        return $this->marksteines;
    }
}
