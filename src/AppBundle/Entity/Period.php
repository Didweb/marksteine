<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Period
 *
 * @ORM\Table(name="period")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PeriodRepository")
 */
class Period
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="yearInit", type="integer")
     */
    private $yearInit;

    /**
     * @var int
     *
     * @ORM\Column(name="monthInit", type="integer")
     */
    private $monthInit;

    /**
     * @var int
     *
     * @ORM\Column(name="dayInit", type="integer")
     */
    private $dayInit;

    /**
     * @var int
     *
     * @ORM\Column(name="yearEnd", type="integer")
     */
    private $yearEnd;

    /**
     * @var int
     *
     * @ORM\Column(name="monthEnd", type="integer")
     */
    private $monthEnd;

    /**
     * @var int
     *
     * @ORM\Column(name="dayEnd", type="integer")
     */
    private $dayEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Period
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set yearInit
     *
     * @param integer $yearInit
     *
     * @return Period
     */
    public function setYearInit($yearInit)
    {
        $this->yearInit = $yearInit;

        return $this;
    }

    /**
     * Get yearInit
     *
     * @return int
     */
    public function getYearInit()
    {
        return $this->yearInit;
    }

    /**
     * Set monthInit
     *
     * @param integer $monthInit
     *
     * @return Period
     */
    public function setMonthInit($monthInit)
    {
        $this->monthInit = $monthInit;

        return $this;
    }

    /**
     * Get monthInit
     *
     * @return int
     */
    public function getMonthInit()
    {
        return $this->monthInit;
    }

    /**
     * Set dayInit
     *
     * @param integer $dayInit
     *
     * @return Period
     */
    public function setDayInit($dayInit)
    {
        $this->dayInit = $dayInit;

        return $this;
    }

    /**
     * Get dayInit
     *
     * @return int
     */
    public function getDayInit()
    {
        return $this->dayInit;
    }

    /**
     * Set yearEnd
     *
     * @param integer $yearEnd
     *
     * @return Period
     */
    public function setYearEnd($yearEnd)
    {
        $this->yearEnd = $yearEnd;

        return $this;
    }

    /**
     * Get yearEnd
     *
     * @return int
     */
    public function getYearEnd()
    {
        return $this->yearEnd;
    }

    /**
     * Set monthEnd
     *
     * @param integer $monthEnd
     *
     * @return Period
     */
    public function setMonthEnd($monthEnd)
    {
        $this->monthEnd = $monthEnd;

        return $this;
    }

    /**
     * Get monthEnd
     *
     * @return int
     */
    public function getMonthEnd()
    {
        return $this->monthEnd;
    }

    /**
     * Set dayEnd
     *
     * @param integer $dayEnd
     *
     * @return Period
     */
    public function setDayEnd($dayEnd)
    {
        $this->dayEnd = $dayEnd;

        return $this;
    }

    /**
     * Get dayEnd
     *
     * @return int
     */
    public function getDayEnd()
    {
        return $this->dayEnd;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Period
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
